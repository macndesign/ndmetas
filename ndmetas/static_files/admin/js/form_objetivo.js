(function($) {
    $(document).ready(function($) {
        $("input").attr("disabled", true);
        $("select").attr("disabled", true);
        $("textarea").attr("disabled", true);
        $(":submit").attr("disabled", false).click(function(){
            $("input").attr("disabled", false);
            $("select").attr("disabled", false);
            $("textarea").attr("disabled", false);
        });
        $(".field-quantidade_atual").find(":text").attr("disabled", false);
    });
})(django.jQuery);