function pie(ctx, w, h, datalist) {
    var radius = h / 2 - 5;
    var centerx = w / 2;
    var centery = h / 2;
    var total = 0;
    for (x = 0; x < datalist.length; x++) {
        total += datalist[x];
    }
    var lastend = 0;
    var offset = Math.PI / 2;
    for (x = 0; x < datalist.length; x++) {
        var thispart = datalist[x];
        ctx.beginPath();
        ctx.fillStyle = colist[x];
        ctx.moveTo(centerx, centery);
        var arcsector = Math.PI * (2 * thispart / total);
        ctx.arc(centerx, centery, radius, lastend - offset, lastend + arcsector - offset, false);
        ctx.lineTo(centerx, centery);
        ctx.fill();
        ctx.closePath();
        lastend += arcsector;
    }
}

function circle(ctx, w, h, percent, color, bg_color) {
    ctx.beginPath();
    ctx.fillStyle = bg_color;
    ctx.arc(w / 2, h / 2, w / 4, 0, 2 * Math.PI, false);
    ctx.fill();
    ctx.fillStyle = color;
    ctx.font = "bold 14px sans-serif";
    ctx.textAlign = "center";
    ctx.fillText(percent + "%", w / 2, h / 1.8);
}

var knobs = document.getElementsByClassName('graph-knob');
for (var i = 0; i < knobs.length; i++){
    var newCanvas = document.createElement("canvas");
    newCanvas.width = 100;
    newCanvas.height = 100;
    newCanvas.id = knobs[i].dataset.ident;
    newCanvas.dataset.color = knobs[i].dataset.color;
    newCanvas.dataset.percent = knobs[i].dataset.percent;
    knobs[i].appendChild(newCanvas);

    // Canvas
    var canvas = document.getElementById(newCanvas.id);
    var ctx = canvas.getContext("2d");

    // Data
    var percent = parseInt(canvas.dataset.percent);
    var color = canvas.dataset.color;
    var bg_color = "#ddd";

    // Pie
    var datalist = [percent, 100 - percent];
    var colist = [color, bg_color];
    pie(ctx, canvas.width, canvas.height, datalist);

    // Circle
    circle(ctx, canvas.width, canvas.height, percent, color, "#fff");
}
