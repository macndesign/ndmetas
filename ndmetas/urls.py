from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('core.urls', namespace='objetivo')),
    url(r'^admin/', include(admin.site.urls)),

    # Accounts
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^accounts/change-password/$', 'django.contrib.auth.views.password_change', {'post_change_redirect': '/'}, name='change_password'),
    url(r'^accounts/change-password/done/$', 'django.contrib.auth.views.password_change_done', name='change_password_done'),
)

urlpatterns += staticfiles_urlpatterns()
