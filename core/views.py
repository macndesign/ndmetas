from django.views.generic.list import ListView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from core.models import Objetivo, ItemObjetivo
from django.shortcuts import render, redirect

# Create your views here.
class ObjetivoListView(ListView):
    queryset = Objetivo.ativos.all()

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ObjetivoListView, self).dispatch(*args, **kwargs)


class ObjetivoMobListView(ListView):
    queryset = Objetivo.ativos.all()
    template_name = 'core/objetivo_mob_list.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ObjetivoMobListView, self).dispatch(*args, **kwargs)


class Objetivo2ListView(ListView):
    queryset = Objetivo.ativos.all()
    template_name = 'core/objetivo_new_list.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(Objetivo2ListView, self).dispatch(*args, **kwargs)


# Audit log page
@login_required
def audit_metas(request):
    objetivos = Objetivo.objects.all()
    item_objetivos = ItemObjetivo.objects.all()

    return render(request,
        'core/objetivos_audit_metas.html',
        {'objetivos': objetivos, 'item_objetivos': item_objetivos}
    )
