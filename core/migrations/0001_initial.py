# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Objetivo'
        db.create_table('core_objetivo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titulo', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('descricao', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('data_inicio', self.gf('django.db.models.fields.DateField')()),
            ('data_fim', self.gf('django.db.models.fields.DateField')()),
            ('alterado_por', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('data_criacao', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('data_atualizacao', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('core', ['Objetivo'])

        # Adding model 'Servico'
        db.create_table('core_servico', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nome', self.gf('django.db.models.fields.CharField')(max_length=120)),
        ))
        db.send_create_signal('core', ['Servico'])

        # Adding model 'Medida'
        db.create_table('core_medida', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('unidade', self.gf('django.db.models.fields.CharField')(max_length=75)),
        ))
        db.send_create_signal('core', ['Medida'])

        # Adding model 'ItemObjetivo'
        db.create_table('core_itemobjetivo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('objetivo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Objetivo'])),
            ('servico', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Servico'])),
            ('quantidade', self.gf('django.db.models.fields.IntegerField')()),
            ('quantidade_atual', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('medida', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Medida'])),
        ))
        db.send_create_signal('core', ['ItemObjetivo'])


    def backwards(self, orm):
        # Deleting model 'Objetivo'
        db.delete_table('core_objetivo')

        # Deleting model 'Servico'
        db.delete_table('core_servico')

        # Deleting model 'Medida'
        db.delete_table('core_medida')

        # Deleting model 'ItemObjetivo'
        db.delete_table('core_itemobjetivo')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.itemobjetivo': {
            'Meta': {'object_name': 'ItemObjetivo'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medida': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Medida']"}),
            'objetivo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Objetivo']"}),
            'quantidade': ('django.db.models.fields.IntegerField', [], {}),
            'quantidade_atual': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'servico': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Servico']"})
        },
        'core.medida': {
            'Meta': {'object_name': 'Medida'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unidade': ('django.db.models.fields.CharField', [], {'max_length': '75'})
        },
        'core.objetivo': {
            'Meta': {'object_name': 'Objetivo'},
            'alterado_por': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'}),
            'data_atualizacao': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'data_criacao': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_fim': ('django.db.models.fields.DateField', [], {}),
            'data_inicio': ('django.db.models.fields.DateField', [], {}),
            'descricao': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        'core.servico': {
            'Meta': {'object_name': 'Servico'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nome': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['core']