from django.conf.urls import patterns, url
from core.views import ObjetivoListView, Objetivo2ListView, ObjetivoMobListView

urlpatterns = patterns('',
    url(r'^$', ObjetivoListView.as_view(), name='dash'),
    url(r'^mob/$', ObjetivoMobListView.as_view(), name='mob'),
    url(r'^list/$', Objetivo2ListView.as_view(), name='list'),
    # Audit log
    url(r'^audit-metas/$', 'core.views.audit_metas', name='audit_metas'),
)
