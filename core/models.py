# coding: utf-8
from django.contrib.auth.models import User
from django.db import models
from audit_log.models.fields import LastUserField
from audit_log.models.managers import AuditLog


# South introspection rules for django-audit-log
from south.modelsinspector import add_introspection_rules

try:
    # Make sure the `to` and `null` parameters will be ignored
    rules = [(
                 (LastUserField,),
                 [],
                 {
                     'to': ['rel.to', {'default': User}],
                     'null': ['null', {'default': True}],
                     },
                 )]

    # Add the rules for the `LastUserField`
    add_introspection_rules(
        rules,
        ['^audit_log\.models\.fields\.LastUserField'],
    )
except ImportError:
    pass


# Create your models here.
class ObjetivoAtivoManager(models.Manager):
    def get_query_set(self):
        return super(ObjetivoAtivoManager, self).get_query_set().filter(ativo=True)

class Objetivo(models.Model):
    titulo = models.CharField(u'Título', max_length=120)
    descricao = models.TextField(blank=True)
    data_inicio = models.DateField(u'Data de início')
    data_fim = models.DateField(u'Data de conclusão')
    ativo = models.BooleanField(default=True)

    # Auditoria
    alterado_por = models.ForeignKey(User, verbose_name='Alterado por', editable=False)
    data_criacao = models.DateTimeField(u'Data de criação', auto_now_add=True)
    data_atualizacao = models.DateTimeField(u'Data de atualização', auto_now=True)

    # Manager
    objects = models.Manager()
    ativos = ObjetivoAtivoManager()

    # Log
    audit_log = AuditLog()

    class Meta:
        ordering = ['id']

    def __unicode__(self):
        return self.titulo


class Servico(models.Model):
    nome = models.CharField(max_length=120)

    # Log
    audit_log = AuditLog()

    def __unicode__(self):
        return self.nome


class Medida(models.Model):
    unidade = models.CharField(max_length=75)

    # Log
    audit_log = AuditLog()

    def __unicode__(self):
        return self.unidade


class ItemObjetivo(models.Model):
    objetivo = models.ForeignKey(Objetivo)
    servico = models.ForeignKey(Servico)
    quantidade = models.IntegerField()
    quantidade_atual = models.IntegerField('Quantidade atual', default=0)
    medida = models.ForeignKey(Medida)

    # Log
    audit_log = AuditLog()

    def porcentagem_concluida(self):
        percentual = self.quantidade_atual * 100 / self.quantidade
        return '{0}'.format(int(percentual))

    def cor_knob(self):

        CORES = {
            'vermelho': '#FF1539',
            'amarelo': '#FFDB15',
            'verde': '#29BC10',
            'dourado': '#CBAE0C',
        }

        if int(self.porcentagem_concluida()) <= 30:
            return CORES['vermelho']
        elif 30 < int(self.porcentagem_concluida()) <= 75:
            return CORES['amarelo']
        elif 75 < int(self.porcentagem_concluida()) < 100:
            return CORES['verde']
        else:
            return CORES['dourado']

    def __unicode__(self):
        return u'{0}% Concluído'.format(float(self.porcentagem_concluida()))
