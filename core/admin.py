from django.contrib import admin
from core.models import Objetivo, ItemObjetivo, Servico, Medida


class ItemObjetivoAdmin(admin.TabularInline):
    model = ItemObjetivo
    extra = 0


class ObjetivoAdmin(admin.ModelAdmin):
    inlines = [ItemObjetivoAdmin]
    list_display = ['titulo', 'data_inicio', 'data_fim', 'alterado_por', 'data_criacao', 'data_atualizacao']

    def save_model(self, request, obj, form, change):
        obj.alterado_por = request.user
        obj.save()


admin.site.register(Objetivo, ObjetivoAdmin)
admin.site.register([Servico, Medida])
