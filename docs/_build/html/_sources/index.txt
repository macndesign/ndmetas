.. ND Metas documentation master file, created by
   sphinx-quickstart on Fri Jan 18 15:02:38 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentação ND Metas
=====================

Conteúdos:

.. toctree::
   :maxdepth: 2

   _contents/diagramas
   _contents/presentation-audit-log



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

