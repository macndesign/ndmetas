Rápida apresentação da app django-audit-log para auditoria de modelos no django
===============================================================================

Importando a class Objetivo e listando todas as metas cadastradas

.. code-block:: python

    >>> from core.models import Objetivo
    >>> Objetivo.objects.all()
    [<Objetivo: Meta 1º Semestre>, <Objetivo: Meta exclusiva para o setor de Projetos (Bello)>]



Pegando o Objetivo (Meta) com ID 1 que é o objeto ``<Objetivo: Meta 1º Semestre>``

.. code-block:: python

    >>> ob1 = Objetivo.objects.get(pk=1)
    >>> ob1
    <Objetivo: Meta 1º Semestre>



Conferindo e verificando objeto capturado
-----------------------------------------

Conferindo o campo audit_log do objeto ``ob1`` para observar a descrição no próprio objeto

.. code-block:: python

    >>> ob1.audit_log.all()
    [<ObjetivoAuditLogEntry: Objetivo: Meta 1º Semestre changed at 2013-01-17 12:37:10.308000+00:00>]



Verificando se todos os itens do objetivo contém um objeto do tipo audit-log

.. code-block:: python

    >>> for a in ob1.itemobjetivo_set.all():
    ...     print a.audit_log
    ...
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>
    <audit_log.models.managers.AuditLogManager object at 0x023EC690>



Percorrendo todos os itens do objeto capturado
----------------------------------------------

Percorrendo todos os itens de objetivos pra verificar quais foram criados, excluídos ou alterados

.. code-block:: python

    >>> for a in ob1.itemobjetivo_set.all():
    ...     print a.audit_log.all()
    ...
    [<ItemObjetivoAuditLogEntry: ItemObjetivo: 50.0% Concluído changed at 2013-01-17 12:37:10.315000+00:00>]
    []
    [<ItemObjetivoAuditLogEntry: ItemObjetivo: 96.0% Concluído changed at 2013-01-17 12:37:10.322000+00:00>]
    []
    []
    []
    []
    []
    []



Obtendo dados mais específicos sobre o objeto, note que cada tabela audit_log tem uma cópia de todos os dados do objeto
no momento da criação, alteração ou remoção do mesmo

.. code-block:: python

    >>> for a in ob1.itemobjetivo_set.all():
    ...     for i in a.audit_log.all():
    ...         print i.id, i.action_id, i.action_user, i.action_date, i.action_type, i.quantidade, i.quantidade_atual
    ...
    1 1 admin 2013-01-17 12:37:10.315000+00:00 U 8 4
    3 2 admin 2013-01-17 12:37:10.322000+00:00 U 54 52



Com isso consigo identificar que:

====  =========  =======  ================================  ============  ================  ======================
ID    ID do Log  Usuário  Data/Hora da ação                 Tipo da ação  Campo Quantidade  Campo Quantidade atual
====  =========  =======  ================================  ============  ================  ======================
1     1          admin    2013-01-17 12:37:10.315000+00:00  U             8                 4
3     2          admin    2013-01-17 12:37:10.322000+00:00  U             54                52
====  =========  =======  ================================  ============  ================  ======================



Comparativo após alteração dos dados de uma determinada meta
------------------------------------------------------------

Após alterar os mesmos campos tenho o seguinte resultado:

.. code-block:: python

    >>> for a in ob1.itemobjetivo_set.all():
    ...     for i in a.audit_log.order_by('action_id'):
    ...         print i.id, i.action_id, i.action_user, i.action_date, i.action_type, i.quantidade, i.quantidade_atual
    ...
    1 1 admin 2013-01-17 12:37:10.315000+00:00 U 8 4
    1 3 admin 2013-01-17 14:43:17.915000+00:00 U 8 5
    3 2 admin 2013-01-17 12:37:10.322000+00:00 U 54 52
    3 4 admin 2013-01-17 14:43:17.923000+00:00 U 54 53



Com isso consigo identificar que:

====  =========  =======  ================================  ============  ================  ======================
ID    ID do Log  Usuário  Data/Hora da ação                 Tipo da ação  Campo Quantidade  Campo Quantidade atual
====  =========  =======  ================================  ============  ================  ======================
1     1          admin    2013-01-17 12:37:10.315000+00:00  U             8                 4
1     3          admin    2013-01-17 14:43:17.915000+00:00  U             8                 5
3     2          admin    2013-01-17 12:37:10.322000+00:00  U             54                52
3     4          admin    2013-01-17 14:43:17.923000+00:00  U             54                53
====  =========  =======  ================================  ============  ================  ======================

