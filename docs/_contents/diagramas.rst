Modelo de dados do sistema de Metas
===================================

Modelo básico sem campos de auditoria e log de objetos
------------------------------------------------------

.. figure:: ../_images/diagram.png
    :align: center

    Diagrama de modelos (focado apenas no negócio)



Diagrama de dependencia de modelos
----------------------------------

.. figure:: ../_images/diagram-model-dependency.png
    :align: center

    Diagrama de dependencia de modelos (focado em classes do negócio)



Diagrama de Heraça
------------------

.. figure:: ../_images/diagram-dependency.png
    :align: center

    Diagrama de dependencia (herança de classes)


